window.addEventListener("DOMContentLoaded", function () {

  const serverData = 'https://jsonplaceholder.typicode.com/comments';
  const dataBtn = document.querySelector('#data-recive-btn');
  const allComments = document.querySelector('.all-comments');
  const paginationWrap = document.querySelector(".pagination-wrapper");
  let myData = [];
  let curentPage = [];
  let pageCount;

  function clearAllData() {
    paginationWrap.innerHTML = '';
    allComments.innerHTML = '';
    myData = [];
    curentPage = [];
    pageCount = undefined;
  }

  function setPageCount() {
    pageCount = Math.ceil(myData.length / 10);
    paginationWrap.innerText = '';
    let string = `<span>prev</span>`
    paginationWrap.insertAdjacentHTML("beforeend", string);
    for (let i = 0; i < pageCount; i++) {
      string = `<span>${i + 1}</span>`
      paginationWrap.insertAdjacentHTML("beforeend", string);
    }
    string = `<span>next</span>`
    paginationWrap.insertAdjacentHTML("beforeend", string);
  }

  function setPaginationStyle(elem) {
    if (!elem) {
      curentPage[1].classList.remove('_active');
    } else {
      elem.classList.add('_active');
    }
  }

  function paginationHandler(elem) {
    if (!elem && curentPage == false) {
      curentPage[0] = 1;
      curentPage[1] = paginationWrap.firstElementChild.nextElementSibling;
      setPaginationStyle(curentPage[1])
      return;
    }
    if ((elem.innerText === "prev") && (curentPage[0] > 1)) {
      setPaginationStyle();
      --curentPage[0];
      curentPage[1] = curentPage[1].previousElementSibling;
      setPaginationStyle(curentPage[1]);
      show(myData, curentPage[0]);
      return;
    }
    else if ((elem.innerText === "next") && (curentPage[0] < pageCount)) {
      setPaginationStyle();
      ++curentPage[0];
      curentPage[1] = curentPage[1].nextElementSibling;
      setPaginationStyle(curentPage[1]);
      show(myData, curentPage[0]);
      return;
    }
    else if (!(elem.innerText === "next") && !(elem.innerText === "prev") && !(elem.innerText == curentPage[0])) {
      setPaginationStyle();
      curentPage[0] = elem.innerText;
      curentPage[1] = elem;
      setPaginationStyle(curentPage[1]);
      show(myData, curentPage[0]);
      return;
    }
  }

  function req(method = "GET", url = "", callback) {
    clearAllData();
    document.querySelector(".loader-wrapper").classList.add("_active");
    const r = new XMLHttpRequest();
    r.open(method, url);
    r.send();
    r.addEventListener("readystatechange", () => {
      if (r.readyState === 4 && r.status >= 200 && r.status < 300) {
        setTimeout(() => {
          myData = JSON.parse(r.responseText);
          setPageCount();
          paginationHandler();
          callback(myData, curentPage[0]);
          document.querySelector(".loader-wrapper").classList.remove("_active");

        }, 1500)
      } else if (r.readyState === 4) {
        console.error("Помилка з запитом");
      }
    });
  }

  const show = (array, page) => {
    allComments.innerHTML = '';
    array.forEach((element, i) => {
      if (i < (page * 10) && i > (page * 10 - 11)) {
        const string = `
              <div class="comment-container">
              <span>${element.id}</span>
              <div class="name-email">
                <span class="name">${element.name}</span>
                <span class="email">${element.email}</span>
             </div>
              <div class="text">${element.body}</div>
              </div>
             `
        allComments.insertAdjacentHTML("beforeend", string);
      }
    });
  }

  dataBtn.addEventListener('click', () => {
    req("GET", serverData, show);
  });

  paginationWrap.addEventListener('click', (e) => {
    if (e.target.tagName === 'SPAN') {
      paginationHandler(e.target);
    }
  });
});



